#ifndef __DCU_CAN_H__
#define __DCU_CAN_H__

/* CAN Interface for Digital Control Unit */

#include <stdbool.h>
#include <stdint.h>
#include "can-ids/CAN_IDs.h"

/* Message IDs */
#define MSG_ID_DCU_STATUS			32
#define MSG_ID_DCU_CURRENT 			33

/* Sub Structs */

/* Message Structs */
// MSG_ID_DCU_STATUS
// Sends Fuse Detection, 3 point Shutdown Circuit detection, RTR, BL and RTDS state
typedef struct {
	// data[0]
	union {
		struct {
			bool FB_BL :1;
			bool FB_CAN_E :1;
			bool FB_DRS :1;
			bool FB_CUB :1;
			bool FB_fans :1;
			bool FB_pumps :1;
			bool FB_EM :1;
			bool FB_SC_origin :1;
			bool FB_CUA :1;
			bool FB_TSAL :1;
			bool FB_fans_AMS :1;
			bool FB_AMS :1;
			// Shutdown Circuit detection points
			bool SC_BSPD_to_bat: 1;
			bool SC_MH_to_front: 1;
			bool SC_front_to_BSPD: 1;
		};
		uint16_t FB_SC_Detect; // FuseBox and Shutdown Circuit Detect
	};

	// data[1]
	union {
		struct {
			bool CUA_sig :1;
			bool CUB_sig :1;
			bool fan_sig :1;
			bool SC_switch_sig :1;
			bool pump1_sig :1;
			bool pump2_sig :1;
			bool dcdc_switch_sig :1;
			bool BL_sig :1;
			bool buzz_sig :1;
			uint8_t DRS_PWM :7;
		};
		uint16_t Signals;
	};

	// data[2]
	uint16_t TEMP_PIC;

	// data[3]
	uint16_t TEMP_BOX;
} DCU_MSG_Status;

// DCU_MSG_ID_CURRENT
typedef struct {
	/*data[0]*/
	uint16_t LV_current;

	/*data[1]*/
	uint16_t HV_current;
} DCU_MSG_Current;

// MAIN STRUCT
typedef struct {
	DCU_MSG_Status status;
	DCU_MSG_Current current;
} DCU_CAN_Data;

void parse_dcu_message_status(uint16_t data[4], DCU_MSG_Status *status);
void parse_dcu_message_current(uint16_t data[4], DCU_MSG_Current *current);
void parse_can_dcu(CANdata message, DCU_CAN_Data *data);

#endif
