#include <stdint.h>
#include "../CAN_IDs.h"
#include "ARM_CAN.h"

void parse_ARM_torque_message(uint16_t data[4], ARM_torque *torque){
	torque->FL=data[0];
	torque->FR=data[1];
	torque->RL=data[2];
	torque->RR=data[3];
}

void parse_ARM_status_message(uint16_t data[4], ARM_status *status){
	status->ARM_OK=data[0];
}

void parse_ARM_control_info_message(uint16_t data[4], ARM_control_info *control_info){
	control_info->mode = data[0];
	control_info->gain = data[1];
	control_info->tl = data[2];
	control_info->arm_ok = data[3];

	return;
}

void parse_ARM_lims_message(uint16_t data[4], ARM_lims *lims){
    lims->lim_rpm = data[0];
    lims->lim_torque_r = data[1];
    lims->lim_torque_f = data[2];
    
    return;
}

void parse_can_ARM(CANdata message, ARM_CAN_data *data){

	switch(message.msg_id){
		case MSG_ID_ARM_TORQUE : 
			parse_ARM_torque_message(message.data, &(data->torque));
			return;
		case MSG_ID_ARM_STATUS :
			parse_ARM_status_message(message.data, &(data->status));
			return;
		case MSG_ID_ARM_CONTROL_INFO:
			parse_ARM_control_info_message(message.data, &(data->control_info));
			return;

		case MSG_ID_ARM_LIMS :
		    parse_ARM_lims_message(message.data, &(data->lims));
		    return;

		default : return;
		}
}