#ifndef _STEER_CAN_H
#define _STEER_CAN_H

#include "can-ids/CAN_IDs.h"

#include <stdint.h>
#include <stdbool.h>

#define MSG_ID_STEER_SIG   33

typedef enum{
    STEER_LEFT,
    STEER_CENTRE,
    STEER_RIGHT

}INTERFACE_MSG_STEER_THRESHOLDS;

typedef struct{

    uint16_t time_stamp;
    int16_t value;
    bool clear_interface;

}STEER_MSG_SIG;

void parse_can_steer(CANdata message, STEER_MSG_SIG *steer);

#endif
