#ifndef __AHRS_CAN_H__
#define __AHRS_CAN_H__

#include "can-ids/CAN_IDs.h"
#include <stdint.h>

#define MSG_ID_AHRS_HEALTH_TEMPERATURE	32
#define MSG_ID_AHRS_GYRO_Z_ACCEL_X      33
#define MSG_ID_AHRS_GYRO_X_Y            34
#define MSG_ID_AHRS_ACCEL_Y_Z           35
#define MSG_ID_AHRS_MAG                 36
#define MSG_ID_AHRS_QUAT            	37
#define MSG_ID_AHRS_EULER_ANGLE         38
#define MSG_ID_AHRS_EULER_RATE          39

#define GYRO_ABSURD                     330
#define ACCEL_ABSURD                    6

/* DREG_HEALTH -    reports the current status of the GPS 
                    module and the other sensors on the UM7 */
typedef struct {                                                      
    union{
        struct{
            uint8_t OVF :1;     /* This bit is set if the UM7 is attempting to transmit data over 
                                the serial port faster than is allowed given the baud-rate */
            uint8_t MG_N :1;    /*This bit is set if the sensor detects that the norm of the
                                magnetometer measurement is too far away from 1.0 to be 
                                trusted. Usually indicates bad calibration, local field
                                distortions, or both. */
            uint8_t ACC_N :1;   /*This bit is set if the sensor detects that the norm of the
                                accelerometer measurement is too far away from 1G to be
                                used */
            uint8_t ACCEL :1;   /*accelerometer fails to initialize on startup */
            uint8_t GYRO :1;    /* rate gyro fails to initialize on startup */
            uint8_t MAG :1;     /* magnetometer fails to initialize on startup */
        };
        uint8_t health;
    };
    uint16_t temperature_time;
    float temperature;          /*temperature output of the onboard temperature sensor */

}AHRS_MSG_HEALTH_TEMPERATURE;

/* GYRO -           measured angular rate in degrees/s 
                    after calibration has been applied */
typedef struct{
    float x;                     /* angular rates in degrees per second */
    float y;
    float z;
    uint16_t time_z;            /* time at witch last value of z was acquired */
    uint16_t time_x_y;          /* time at witch last value of x and y was acquired */

}AHRS_MSG_GYRO;

/* ACCEL -          measured acceleration in m/s/s 
                    after calibration has been applied */
typedef struct{
    float x;                    /*  acceleration in m/s/s */
    float y;
    float z;
    uint16_t time_x;            /* time at witch last value of x was acquired */
    uint16_t time_y_z;          /* time at witch last value of y and z was acquired */

}AHRS_MSG_ACCEL;

/* MAG -            measured magnetic field 
                    after calibration has been applied */
typedef struct{
    float x;                    /*  measured magnetic field */
    float y; 
    float z;
    uint16_t time;              /* time at witch last value was acquired */

}AHRS_MSG_MAG;

/* QUAT -            estimated quaternion attitude*/
typedef struct{
    float A;                    /* First estimated quaternion component attitude */
    float B;                    /* Second estimated quaternion component attitude */
    float C;                    /* Third estimated quaternion component attitude */
    float D;                    /* Fourth estimated quaternion component attitude */
    uint16_t time;              /* time at witch last value was acquired */
                           
}AHRS_MSG_QUAT;

/* EULER -            */
typedef struct{
    float roll;                 /* roll angle estimate */
    float pitch;                /* pitch angle estimate */
    float yaw;                  /* yaw angle estimate */
    float roll_rate;            /* roll rate estimate */
    float pitch_rate;           /* pitch rate estimate */
    float yaw_rate;             /* yaw rate estimate */
    uint16_t time_angle;        /* time at witch last value of angles was acquired */
    uint16_t time_rate;         /* time at witch last value of rates was acquired */

}AHRS_MSG_EULER;


// MAIN STRUCT
typedef struct {
    AHRS_MSG_HEALTH_TEMPERATURE health_temperature;
    AHRS_MSG_GYRO gyro;
    AHRS_MSG_ACCEL accel;
    AHRS_MSG_MAG mag;
    AHRS_MSG_QUAT quat;
    AHRS_MSG_EULER euler;
}AHRS_CAN_Data;

void parse_can_message_health_temperature(uint16_t data[4], AHRS_MSG_HEALTH_TEMPERATURE *health_temperature);
void parse_can_message_gyro_z_accel_x(uint16_t data[4], AHRS_MSG_GYRO *gyro, AHRS_MSG_ACCEL *accel);
void parse_can_message_gyro_x_y(uint16_t data[4], AHRS_MSG_GYRO *gyro);
void parse_can_message_accel_y_z(uint16_t data[4], AHRS_MSG_ACCEL *accel);
void parse_can_message_mag(uint16_t data[4], AHRS_MSG_MAG *mag);
void parse_can_message_quat(uint16_t data[4], AHRS_MSG_QUAT *quat);
void parse_can_message_euler_angle(uint16_t data[4], AHRS_MSG_EULER *euler);
void parse_can_message_euler_rate(uint16_t data[4], AHRS_MSG_EULER *euler);
void parse_can_ahrs(CANdata message, AHRS_CAN_Data *data);

#endif