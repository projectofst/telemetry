#include "GPS_CAN.h"

void parse_can_gps (CANdata message, GPS_PARSED_DATA *data){

    parse_gps_message_clock       (message, data);
    parse_gps_message_velocity    (message, data);
    parse_gps_message_coordinates (message, data);

    return;
} 

void parse_gps_message_clock (CANdata message, GPS_PARSED_DATA *data){

    if(message.dev_id != DEVICE_ID_GPS){
        return;
    }
    else if(message.msg_id == MSG_ID_GPS_CLOCK_SYNC){

        data->miliseconds = message.data[1];
        return;
    }
    else    
        return;
}

void parse_gps_message_velocity (CANdata message, GPS_PARSED_DATA *data){

    if(message.dev_id != DEVICE_ID_GPS){
        return;
    }
    else if(message.msg_id == MSG_ID_GPS_VELOCITY){

        data->ground_speed = message.data[1];
        return;
    }
    else
        return;
}

void parse_gps_message_coordinates (CANdata message, GPS_PARSED_DATA *data){

    if(message.dev_id != DEVICE_ID_GPS){
        return;
    }
    else if(message.msg_id == MSG_ID_GPS_LATITUDE){

        data->lat_dg      = message.data[1];   
        data->lat_min     = message.data[2];
        data->lat_dec_min = message.data[3];

        return;
    }
    else if(message.msg_id == MSG_ID_GPS_LONGITUDE){

        data->long_dg      = message.data[1];
        data->long_min     = message.data[2];
        data->long_dec_min = message.data[3];

        return;
    }
    else
        return;
}
