#ifndef __ARM_CAN_H__
#define __ARM_CAN_H__

/*Can interface for IIB*/

#include <stdint.h>
#include "../CAN_IDs.h"

//Message ids 

typedef enum {
    MODE_TRACTION_LIMIT,
    MODE_DIFFERENTIAL,
    MODE_NONE,
} CONTROLLER_MODE;

typedef struct{
	uint16_t FL;
	uint16_t FR;
	uint16_t RL;
	uint16_t RR;
}ARM_torque;

#define MSG_ID_ARM_STATUS 35

typedef struct{
	uint8_t ARM_OK;

}ARM_status;

#define MSG_ID_ARM_LIMS 		36

typedef struct{
    uint16_t lim_rpm;
    uint16_t lim_torque_f;
    uint16_t lim_torque_r;
}ARM_lims;

#define MSG_ID_ARM_CONTROL_INFO  37

typedef struct{
    uint16_t mode;
    uint16_t gain;
    uint16_t tl;     /*value *10 */	
    uint8_t  arm_ok;	
}ARM_control_info;





//CAN structure 

typedef struct{
	ARM_torque torque;
	ARM_status status;
	ARM_control_info control_info;
	ARM_lims lims;
}ARM_CAN_data;

//parse functions
void parse_ARM_torque_message(uint16_t data[4], ARM_torque *torque);
void parse_ARM_status_message(uint16_t data[4], ARM_status *status);
void parse_ARM_control_info_message(uint16_t data[4], ARM_control_info *control_info);
void parse_ARM_lims_message(uint16_t data[4], ARM_lims *lims);

void parse_can_ARM(CANdata message, ARM_CAN_data *data);
 
#endif
