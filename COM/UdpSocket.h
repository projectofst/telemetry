#ifndef UDPSOCKET_H
#define UDPSOCKET_H

#include "COM.h"

namespace Ui {
    class UdpSocket;
}

class UdpSocket : public COM
{
    Q_OBJECT

public:
    UdpSocket();
    virtual bool open(QString) override;
    virtual int close(void) override;
    virtual std::pair<int, CANdata> pop(void) override;

public slots:
    virtual int send(CANdata) override;
    virtual void read(void) override;

private:
    QUdpSocket *udpsocket;
    QByteArray datagramsend;
    QByteArray datagramreceive;

signals:
    void new_messages(int);
};

#endif // UDPSOCKET_H
