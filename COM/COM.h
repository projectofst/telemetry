#ifndef COM_H
#define COM_H

#include <QObject>
#include <QtSerialPort/QtSerialPort>
#include <QTcpSocket>
#include <QTcpServer>
#include <QtNetwork>
#include <QNetworkSession>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <queue>
#include <thread>
#include <mutex>
#include <QMutex>
#include <QUdpSocket>

#include "can-ids/CAN_IDs.h"

namespace Ui {
    class COM;
}

class COM : public QObject
{
    Q_OBJECT

public:
    COM();
    virtual bool open(QString);
    virtual int close(void);
    virtual int send(CANdata msg);
    virtual std::pair<int, CANdata> pop(void);

public slots:
    virtual void read(void);

private:

};

#endif // COM_H
