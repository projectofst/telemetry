#include "UdpSocket.h"

UdpSocket::UdpSocket()
{
    return;
}

bool UdpSocket::open(QString)
{
    udpsocket = new QUdpSocket(this);

    datagramsend.resize(10);

    connect(udpsocket,SIGNAL(disconnected()),this,SLOT(establishConection()));
    connect(udpsocket,SIGNAL(readyRead(void)),this,SLOT(read(void)));
}

void UdpSocket::read(void)
{
    printf("read\n");
    while (udpsocket->hasPendingDatagrams()) {
        emit new_messages(1);
    }
    emit new_messages(0);
}

int UdpSocket::send(CANdata msg)
{
    printf("send\n");

    printf("%d %d %d %d %d %d\n", msg.sid, msg.dlc, msg.data[0], msg.data[1], msg.data[2], msg.data[3]);
    char *datagram = (char *) malloc(256);
    printf("malloc\n");
    sprintf(datagram, "%d%d%d%d%d\n", msg.sid, msg.dlc, msg.data[0], msg.data[1], msg.data[2]);
    printf("datagram\n");
    QByteArray *some_datagram = new QByteArray(datagram);
    printf("qbytearray\n");

    printf("init datagram");
    if (udpsocket->state() == QAbstractSocket::BoundState)
        udpsocket->writeDatagram(*some_datagram, QHostAddress::Broadcast, 45454);
    printf("send datagram\n");
    return 1;
}

std::pair<int, CANdata> UdpSocket::pop()
{
    CANdata msg;
    QByteArray datagramreceive;
    // while (udpsocket->hasPendingDatagrams()) {
    datagramreceive.resize(int(udpsocket->pendingDatagramSize()));
    udpsocket->readDatagram(datagramreceive.data(),datagramreceive.size());

    qDebug()<<datagramreceive.data();
    //}
}

int UdpSocket::close()
{
    udpsocket->~QUdpSocket();
    delete this;
    return 0;
}
