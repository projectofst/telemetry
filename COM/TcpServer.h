#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "COM/COM.h"

namespace Ui {
    class TcpServer;
}

class TcpServer : public COM
{
    Q_OBJECT

public:
    TcpServer();
    virtual bool open(QString ip) override;
    
private:
    QTcpServer *wifiserver;
    QNetworkSession *networkSession;
    
};

#endif // TCPSERVER_H
